# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the libksysguard package.
# SPDX-FileCopyrightText: 2024 Shinjo Park <kde@peremen.name>
#
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-08 00:39+0000\n"
"PO-Revision-Date: 2024-02-10 00:17+0100\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 23.08.1\n"

#: contents/ui/Config.qml:29
#, kde-format
msgid "Automatic Data Range"
msgstr "자동 데이터 범위"

#: contents/ui/Config.qml:33
#, kde-format
msgid "From:"
msgstr "시작:"

#: contents/ui/Config.qml:40
#, kde-format
msgid "To:"
msgstr "끝:"
