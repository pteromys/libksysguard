# Bulgarian translations for libksysguard package.
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the libksysguard package.
#
# Automatically generated, 2022.
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-07 00:17+0000\n"
"PO-Revision-Date: 2022-07-21 14:03+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.0.1\n"

#: contents/ui/Config.qml:42
#, kde-format
msgid "Appearance"
msgstr "Външен вид"

#: contents/ui/Config.qml:47
#, kde-format
msgid "Show Sensors Legend"
msgstr "Показване на легендата на сензорите"

#: contents/ui/Config.qml:51
#, kde-format
msgid "Stacked Charts"
msgstr "Подредени диаграми"

#: contents/ui/Config.qml:55
#, kde-format
msgid "Smooth Lines"
msgstr "Гладки линии"

#: contents/ui/Config.qml:59
#, kde-format
msgid "Show Grid Lines"
msgstr "Показване на линии"

#: contents/ui/Config.qml:63
#, kde-format
msgid "Show Y Axis Labels"
msgstr "Показване на етикети на оста Y"

#: contents/ui/Config.qml:67
#, kde-format
msgid "Fill Opacity:"
msgstr "Запълване:"

#: contents/ui/Config.qml:73
#, kde-format
msgid "Data Ranges"
msgstr "Диапазони от данни"

#: contents/ui/Config.qml:78
#, kde-format
msgid "Automatic Y Data Range"
msgstr "Автоматичен обхват на данните Y"

#: contents/ui/Config.qml:82
#, kde-format
msgid "From (Y):"
msgstr "От (Y):"

#: contents/ui/Config.qml:89
#, kde-format
msgid "To (Y):"
msgstr "До (Y):"

#: contents/ui/Config.qml:99
#, kde-format
msgid "Amount of History to Keep:"
msgstr "Обем на историята за запазване:"

#: contents/ui/Config.qml:102
#, kde-format
msgctxt "%1 is seconds of history"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 секунда"
msgstr[1] "%1 секунди"
